FROM registry-gitlab.pasteur.fr/tru/docker-c7-ci:17c8b7e5

RUN yum -y update && yum -y upgrade && \
        yum -y install wget bzip2 gcc gcc-c++ gcc-gfortran make \
	xdg-utils mesa-libgbm libdrm libxcb at-spi2-core gtk3 libnotify  && \
	yum -y groupinstall development && \
	yum -y clean all && \
	 date +"%Y-%m-%d-%H%M" > /last_update
RUN wget --quiet https://registrationcenter-download.intel.com/akdlm/IRC_NAS/20f4e6a1-6b0b-4752-b8c1-e5eacba10e01/l_BaseKit_p_2024.0.0.49564_offline.sh && \
    export TERM=dumb   && \
    bash l_BaseKit_p_2024.0.0.49564_offline.sh \
    --log l_BaseKit_p_2024.0.0.49564_offline.log \
    --remove-extracted-files yes \
    -a \
     --action install \
     --cli \
     --eula accept \
     --intel-sw-improvement-program-consent decline \
     --silent && \
     /bin/rm l_BaseKit_p_2024.0.0.49564_offline.sh
